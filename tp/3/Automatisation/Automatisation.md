﻿# Module 6 : Automatiser le déploiement

Bon bah là je vais pas beaucoup écrire pour ce sujet.

C'est simple : automatiser via un script `bash` le TP2. Tout ce que vous avez fait à la main dans le TP2 pour déployer un NextCloud et sa base de données, le but ici est de le faire de façon automatisée *via* un script `bash`.


➜ **Ecrire le script `bash`**

- il s'appellera `tp3_automation_nextcloud.sh`
- le script doit commencer par un *shebang* qui indique le chemin du programme qui exécutera le contenu du script
  - ça ressemble à ça si on veut utiliser `/bin/bash` pour exécuter le contenu de notre script :

```
#!/bin/bash
```

- le script :
  - à partir d'une machine vierge (enfin, notre patron de cours Rocky quoi)
  - il installe complètement Apache et NextCloud
  - on lance le script, et on va prendre un café
- NB : le script ne comportera aucun `sudo` car il sera directement lancé avec les droits de `root`, question de bonnes pratiques :

```bash
$ sudo ./tp3_automation_nextcloud.sh
```

> Il peut être intéressant de tester la fonctionnalité de *snapshot* de VirtualBox afin de sauvegarder une VM de test dans un état vierge, et pouvoir la restaurer facilement dans cet état vierge plus tard.

✨ **Bonus** : faire un deuxième script `tp3_automation_db.sh` qui automatise l'installation de la base de données de NextCloud

Je n'ai pas finit le script mais voici ce que j'ai fais pour l'instant
tp3_script_atomation_nextcloud.sh
```
#!/bin/bash
dnf install httpd
systemctl start httpd
systemctl enable httpd
dnf install mariadb-server
systemctl enable mariadb
systemctl start mariadb
mysql -u root < nextcloud.sql

dnf config-manager --set-enabled crb
dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
dnf module list php
dnf module enable php:remi-8.1 -y
dnf install -y php81-php
dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
mkdir /var/www/tp2_nextcloud/
dnf install unzip
dnf install wget
wget https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
unzip nextcloud-25.0.0rc3.zip -d /var/www/tp2_nextcloud/
```
nextcloud.sql
```
CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'pewpewpew';
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
FLUSH PRIVILEGES;
```

